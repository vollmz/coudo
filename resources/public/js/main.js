/* main.js */

grouplist = [];

function setup_select_groups() {
    $.get("/group/list", null, null, "json")
        .done(function(data, textstatus, jqXHR) {
            console.log(data);
            grouplist = data;
            data.forEach(gname => $("#grouplist").append(`<option value="${gname}">${gname}</option>`));
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            alert("Failed to access server. Please reload this page.");
        });
}

function setup_events() {
    $('#addallbutton').on('click', function() {
        $('#selectinput').val(grouplist.join(' '));
    });

    $('#clearbutton').on('click', function() {
        $('#selectinput').val('');
    });
    
    $('#addbutton').on('click', function() {
        const sgroup = $('#grouplist').val();
        const stext = $('#selectinput').val();
        if(stext.substr(-1) == ' ') {
            $('#selectinput').val(stext + sgroup);
        } else {
            $('#selectinput').val(stext + ' ' + sgroup);
        }
    });

    $('#subbutton').on('click', function() {
        const sgroup = $('#grouplist').val();
        const stext = $('#selectinput').val();
        if(stext.substr(-1) == ' ') {
            $('#selectinput').val(stext + '-' + sgroup);
        } else {
            $('#selectinput').val(stext + ' -' + sgroup);
        }
    });

    $('#plotbutton').on('click', function() {
        plot();
    });

    $('#reloadbutton').on('click', function() {
        location.reload(true);
    });
}

function setup_canvas_size() {
    const mw = $('#mainplot').width();
    const mh = $('#mainplot').height();
    $('#mainplot').attr('width', mw);
    $('#mainplot').attr('height', mh);

    const gw = $('#groupplot').width();
    const gh = $('#groupplot').height();
    $('#groupplot').attr('width', gw);
    $('#groupplot').attr('height', gh);
}

function make_selectgroup_list(splst, initlst) {
    if(splst.length > 0) {
        const first = splst[0];
        const rest = splst.slice(1);
        if(first.at(0) != '-') {
            if(initlst.includes(first)) {
                return make_selectgroup_list(rest, initlst);
            } else {
                return make_selectgroup_list(rest, initlst.concat([first]));
            }
        } else {
            if(!initlst.includes(first.substr(1))) {
                return make_selectgroup_list(rest, initlst);
            } else {
                return make_selectgroup_list(rest, initlst.filter(elm => elm != first.substr(1)));
            }
        }
    } else {
        return initlst;
    }
}

function parse_selectinput_text(tex) {
    const texlst = tex.split(' ').filter(elm => elm.length > 0);
    return make_selectgroup_list(texlst, []);
}

function plot() {
    const stext = $('#selectinput').val();
    const glist = parse_selectinput_text(stext);
    const seppmmode = $('#seppmbox').prop('checked');

    if(glist.length == 0) {
        alert('1つ以上のグループを指定する必要があります');
        return -1;
    } else {
        $("#plotbutton").prop("disabled", true);
        $.get("/data/list",
              {"group": glist.join(",")},
              null,
              "json")
            .done(function(data, textstatus, jqXHR) {
                const totalized_data = totalize(data);
                plot_2(totalized_data, glist, seppmmode);
                make_table(totalized_data, glist);
                make_authortable(data);
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            alert("Failed to access server. Please reload this page.");
        });
        return 0;
    }
}

function groupby(mplst, ky) {
    let rtn = {};
    mplst.forEach(function (itm) {
        const rtnkey = itm[ky];
        if(rtn[rtnkey] === undefined) {
            rtn[rtnkey] = [];
        }
        rtn[rtnkey].push(itm);
    });
    return rtn;
}

function totalize_taglist(lst) {
    if(lst.length > 0) {
        const first = lst[0];
        const plst = lst.filter(itm => itm["val"] >= 0);
        const nlst = lst.filter(itm => itm["val"] < 0);

        const reducer = (p, i2) => p + i2['val'];
        const psum = plst.reduce(reducer, 0);
        const nsum = nlst.reduce(reducer, 0);

        return {"groupname": first['groupname'],
                "tid": first['tid'],
                "tagname": first['tagname'],
                "result": [psum, Math.abs(nsum), Math.abs(psum) + Math.abs(nsum), psum + nsum]};
    }else {
        return {};
    }
}

function totalize(data) {
    const groupbytid = groupby(data, "tid");
    return groupby(Object.keys(groupbytid).map(tid => totalize_taglist(groupbytid[tid])),
                   "groupname");
}

function make_mainchart_data(data, glist, sep) {
    let taglist = [];
    let pdatalist = [];
    let ndatalist = [];
    let adatalist = [];
    let datalist = [];
    
    glist.forEach(function(name) {
        const gdata = data[name]; // list or undef
        if(gdata !== undefined) {
            gdata.forEach(function(tagdata){
                taglist.push(tagdata["tagname"]);
                pdatalist.push(tagdata["result"][0]);
                ndatalist.push(tagdata["result"][1]);
                adatalist.push(tagdata["result"][2]);
                datalist.push(tagdata["result"][3]);
            });
        }
    });

    const pdataset = {
        label: "Positive",
        backgroundColor: "rgba(235, 64, 52, 0.7)",
        data: pdatalist
    };

    const ndataset = {
        label: "Negative",
        backgroundColor: "rgba(96, 66, 245, 0.7)",
        data: ndatalist
    };

    const adataset = {
        label: "Abs Total",
        backgroundColor: "rgba(245, 230, 66, 0.7)",
        data: adatalist
    };

    const dataset = {
        label: "Total",
        backgroundColor: "rgba(104, 173, 83, 0.7)",
        data: datalist
    };

    return {
        labels: taglist,
        datasets: sep ? [pdataset, ndataset, adataset, dataset] : [dataset]
    };
}

function make_groupchart_data(data, glist, sep) {
    let pdatalist = [];
    let ndatalist = [];
    let adatalist = [];
    let datalist = [];

    glist.forEach(function(name) {
        const gdata = data[name];
        if(gdata !== undefined) {
            pdatalist.push(gdata.map(elm => elm["result"]).reduce((s, e) => s + e[0], 0));
            ndatalist.push(gdata.map(elm => elm["result"]).reduce((s, e) => s + e[1], 0));
            adatalist.push(gdata.map(elm => elm["result"]).reduce((s, e) => s + e[2], 0));
            datalist.push(gdata.map(elm => elm["result"]).reduce((s, e) => s + e[3], 0));
        } else {
            pdatalist.push(0);
            ndatalist.push(0);
            adatalist.push(0);
            datalist.push(0);
        }
    });

    const pdataset = {
        label: "Positive",
        backgroundColor: "rgba(235, 64, 52, 0.7)",
        data: pdatalist
    };

    const ndataset = {
        label: "Negative",
        backgroundColor: "rgba(96, 66, 245, 0.7)",
        data: ndatalist
    };

    const adataset = {
        label: "Abs Total",
        backgroundColor: "rgba(245, 230, 66, 0.7)",
        data: adatalist
    };

    const dataset = {
        label: "Total",
        backgroundColor: "rgba(104, 173, 83, 0.7)",
        data: datalist
    };
    
    return {
        labels: glist,
        datasets: sep ? [pdataset, ndataset, adataset, dataset] : [dataset]
    };
}

function plot_2(data, glist, sep) {
    const mainchart_context = $('#mainchart');
    const mainchart = new Chart(mainchart_context, {
        type: 'bar',
        data: make_mainchart_data(data, glist, sep),
        responsive: true,
        options: {
            legend: {
                display: false
            }
        }
    });

    const groupchart_context = $('#groupchart');
    const groupchart = new Chart(groupchart_context, {
        type: 'bar',
        data: make_groupchart_data(data, glist, sep),
        responsive: true,
        options: {
            legend: {
                display: false
            }
        }
    });
}

function make_table(data, glist) {
    $('#datahead').append('<tr><th>Group</th><th>Tag</th><th>total value</th><th>Abs total value</th><th>sum of positive</th><th>sum of negative</th></tr>');
    
    glist.forEach(function(name) {
        const gdata = data[name];
        if(gdata !== undefined) {
            gdata.forEach(function(tagdata){
                const tagname = tagdata['tagname'];
                const aval = tagdata['result'][2];
                const pval = tagdata['result'][0];
                const nval = tagdata['result'][1] * -1;
                const val = tagdata['result'][3];
                $('#databody').append(`<tr><th>${name}</th><th>${tagname}</th><th>${val}</th><th>${aval}</th><th>${pval}</th><th>${nval}</th></tr>`);
            });
        }
    });
    
}

function get_taggroup_map(data) {
    let tagmap = {};
    data.forEach(function(itm){
        tagmap[itm['tagname']] = itm['groupname'];
    });
    return tagmap;
}

function get_author_info(data) {
    let authortable = {};
    data.forEach(function(itm){
        const tag = itm['tagname'];
        const author = itm['author'];
        if(authortable[tag] === undefined) {
            authortable[tag] = new Set();
        }
        authortable[tag].add(author);
    });
    return authortable;
}

function make_authortable(data) {
    const tagmap = get_taggroup_map(data);
    const authorinfo = get_author_info(data);
    const authorinfo_keys = Object.keys(authorinfo);

    $('#authordatahead').append('<tr><th>Group</th><th>Tag</th><th>Author</th></tr>');

    authorinfo_keys.forEach(function(tag){
        const group = tagmap[tag];
        const authors = Array.from(authorinfo[tag]).join(',');
        $('#authordatabody').append(`<tr><th>${group}</th><th>${tag}</th><th>${authors}</th></tr>`);
    });
}

$(function() {
    setup_select_groups();
    setup_events();
});


