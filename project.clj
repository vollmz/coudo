(defproject coudo-server "1.1.1"
  :description "A simple numerical recording tool"
  :url "https://bitbucket.org/vollmz/coudo/src/main/"
  :license {:name "MIT"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [org.clojure/tools.cli "1.0.206"]
                 [ring "1.9.5"]
                 [ring/ring-json "0.5.1"]
                 [metosin/reitit "0.5.17"]
                 [selmer "1.12.49"]
                 [com.github.seancorfield/next.jdbc "1.2.761"]
                 [org.xerial/sqlite-jdbc "3.36.0.3"]
                 [com.github.seancorfield/honeysql "2.2.858"]
                 [com.stuartsierra/component "1.0.0"]]
  :main ^:skip-aot coudo-server.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
