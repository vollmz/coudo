(ns coudo-server.handler
  (:require [ring.util.response :as r]
            [selmer.parser :as selmer]
            [clojure.string :as cstr]
            [clojure.spec.alpha :as s]
            [coudo-server.data-spec :as ds]
            [coudo-server.data :as d]
            [coudo-server.formatter :as formatter]))

(def default-access-key "coudo-default-key")

;; common utilities for handler functions
(defn- res-html [body]
  (-> (r/response body)
      (r/content-type "text/html; charset=utf-8")))

(defn- res-csv [csvtext]
  (-> (r/response csvtext)
      (r/content-type "text/csv; charset=utf-8")))

(defn- res-json [status body]
  {:status status :body body :headers {"Content-Type" "application/json"}})

(defmacro with-accesskey [[config-key req] & body]
  `(let [userdata# (:body ~req)
         akey# (if (string? ~config-key) ~config-key default-access-key)]
     (if (= akey# (get userdata# "accesskey" ""))
       (do ~@body)
       (res-json 403 {:msg "Invalid accesskey"}))))

(defn- to-cljdata [userdata & {:keys [default-data]}]
  (let [data-body (get userdata "data" default-data)]
    (into {} (map (fn [[k v]] [(keyword k) v]) data-body))))

(defmacro let-userdata [[datasym data-spec data] & body]
  `(if (s/valid? ~data-spec ~data)
     (let [~datasym ~data] ~@body)
     (res-json 400 {:msg "Invalid data/data structure"})))

(defn- get-maindata-from-db [db & {:keys [groups]}]
  (let [target-tids (if groups
                      (map (fn [x] (:groups/tid x)) (d/select-taglist-of-groups db groups))
                      (map (fn [x] (:tags/tid x)) (d/select-taglist db)))]
    (map d/unqualified (d/select-maindata db target-tids))))

;; handler and auxiliary functions
(defn top [req]
  (res-html (selmer/render-file "templates/index.html" {})))

(defn download-csv [db req]
  (let [params (:query-params req)
        groupstr (get params "group")
        grouplst (when (string? groupstr)
                   (cstr/split groupstr #","))]
    (res-csv (formatter/to-csv (get-maindata-from-db db :groups grouplst)))))

(defn get-ping [req]
  (res-json 200 {:msg "Access OK"}))

(defn post-ping [config req]
  (with-accesskey [(:access-key config) req]
    (res-json 200 {:msg "Access OK"})))

(s/def ::data-push-data (s/keys :req-un [::ds/author ::ds/val ::ds/tag]))

(defn data-push [db config req]
  (with-accesskey [(:access-key config) req]
    (let-userdata [userdata ::data-push-data (to-cljdata (:body req))]
      (if-let [tid (d/get-tid db (:tag userdata))]
        (if (d/insert-maindata db (assoc userdata :tid tid))
          (res-json 200 {:msg "Pushed"})
          (res-json 400 {:msg "Failed to push data"}))
        (res-json 400 {:msg "Failed to get tag information"})))))

(s/def ::data-update-data (s/keys :req-un [::ds/id]
                                  :opt-un [::ds/author ::ds/val ::ds/tag]))

(defn data-update [db config req]
  (with-accesskey [(:access-key config) req]
    (let-userdata [userdata ::data-update-data (to-cljdata (:body req))]
      (let [target-id (:id userdata)
            update-data (dissoc userdata :id)]
        (if (empty? update-data)
          (res-json 200 {:msg "No update"})
          (let [update-data (if (:tag update-data)
                              (dissoc (assoc update-data
                                             :tid
                                             (d/get-tid db (:tag update-data)))
                                      :tag)
                              update-data)]
            (if (d/update-maindata db target-id update-data)
              (res-json 200 {:msg "Updated"})
              (res-json 400 {:msg "Failed to update"}))))))))

(s/def ::data-delete-data (s/keys :req-un [::ds/id]))

(defn data-delete [db config req]
  (with-accesskey [(:access-key config) req]
    (let-userdata [userdata ::data-delete-data (to-cljdata (:body req))]
      (let [target-id (:id userdata)]
        (if (d/delete-maindata db target-id)
          (res-json 200 {:msg "Deleted"})
          (res-json 400 {:msg "No delete"}))))))

(defn data-list [db req]
  (let [params (:query-params req)
        dataid (if (= (get params "id") "1") true false)
        groupstr (get params "group")
        grouplst (when (string? groupstr)
                   (cstr/split groupstr #","))
        dbdata (get-maindata-from-db db :groups grouplst)]
    (res-json 200
              (if dataid
                dbdata
                (map (fn [c] (dissoc c :id)) dbdata)))))

(defn- plus-maindata [d1 d2]
  (into {} (map (fn [[k v]]
                  (let [v2 (get d2 k)]
                    (if (and (integer? v) (integer? v2))
                      [k (+ v v2)]
                      [k (cond (= v v2) v
                               (list? v) (conj v v2)
                               :else (list v v2))])))
                d1)))

(defn- totalize-maindata [data]
  (let [tag-grouped-data (group-by :tid data)
        totalized-data (group-by :groupname
                                 (map (fn [[k v]] (reduce plus-maindata v))
                                      tag-grouped-data))]
    {:grouped (dissoc totalized-data nil)
     :ungrouped (get totalized-data nil [])}))

(defn data-totalize [db req]
  (let [params (:query-params req)]
    (let [target-tids (map (fn [x] (:tags/tid x)) (d/select-taglist db))]
      (let [result (map d/unqualified (d/select-maindata db target-tids))]
        (res-json 200 (totalize-maindata result))))))

(defn tag-getinfo [db req]
  (let [params (:query-params req)
        tagname (get params "tag")]
    (if tagname
      (let [tid (d/select-tid db tagname)
            groupinfo (d/get-tag-groupinfo db tid)]
        (if tid
          (res-json 200
                    {:group (:groups/name groupinfo)
                     :gid (:groups/gid groupinfo)
                     :tag tagname
                     :tid tid})
          (res-json 200 {})))
      (res-json 400 {:msg "Invalid request"}))))

(defn tag-count [db req]
  (let [params (:query-params req)
        tagname (get params "tag")]
    (if tagname
      (let [tid (d/select-tid db tagname)]
        (if (nil? tid)
          (res-json 400 {:msg (str tagname " is not found")})
          (res-json 200 {:tag tagname :tid tid :count (d/count-tid db tid)}))))))

(defn tag-gc [db config req]
  (with-accesskey [(:access-key config) req]
    (let [tags-count (into {} (map (fn [tid-data] (let [tid (:tags/tid tid-data)]
                                                    [tid (d/count-tid db tid)]))
                                   (d/select-taglist db)))
          target-tids (keys (filter (fn [[k v]] (<= v 0)) tags-count))]
      (res-json 200 (d/delete-tag-and-group-association db target-tids)))))

(s/def ::group-make-data (s/keys ::req-un [::ds/group ::ds/tag]))

(defn group-make [db config req]
  (with-accesskey [(:access-key config) req]
    (let-userdata [userdata ::group-make-data (to-cljdata (:body req))]
      (if-let [tid (d/get-tid db (:tag userdata))]
        (if-let [ginfo (d/get-tag-groupinfo db tid)]
          (if (d/update-group db (:groups/gid ginfo) (assoc userdata :tid tid))
            (res-json 200 {:msg "Updated"})
            (res-json 400 {:msg "Failed to update"}))
          (if (d/insert-group db (assoc userdata :tid tid))
            (res-json 200 {:msg "Updated"})
            (res-json 400 {:msg "Failed to update"})))))))

(defn group-list [db req]
  (let [groups (d/select-grouplist db)]
    (res-json 200 (map (fn [res] (:groups/name res)) groups))))
