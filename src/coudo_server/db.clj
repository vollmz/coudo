(ns coudo-server.db
  (:require [clojure.java.io :as io]
            [next.jdbc :as jdbc]
            [com.stuartsierra.component :as cpt]))

(defn- init-sqlite [ds]
  (jdbc/execute! @ds ["CREATE TABLE tags (tid INTEGER PRIMARY KEY AUTOINCREMENT, tagname TEXT)"])
  (jdbc/execute! @ds ["CREATE TABLE maindata (id INTEGER PRIMARY KEY AUTOINCREMENT, author TEXT, tid INTEGER, val INTEGER, recdate TEXT DEFAULT CURRENT_TIMESTAMP, FOREIGN KEY (tid) REFERENCES tags(tid))"])
  (jdbc/execute! @ds ["CREATE TABLE groups (gid INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, tid INTEGER, FOREIGN KEY (tid) REFERENCES tags(tid))"]))

(defrecord SqliteDb [datasource dbpath]
  cpt/Lifecycle
  (start [this]
    (let [ds (delay (jdbc/get-datasource {:dbtype "sqlite" :dbname dbpath}))]
      (when (not (.exists (io/file dbpath))) 
        (init-sqlite ds)) ; Init tables when first use
      (assoc this :datasource ds)))
  
  (stop [this]
    (assoc this :datasource nil)))
