(ns coudo-server.data-spec
  (:require [clojure.spec.alpha :as s]))

(s/def ::id pos-int?)

(s/def ::author (s/and string?
                       #(re-matches #"^[a-zA-Z0-9\_][-a-zA-Z0-9 \_\:]*" %)))

(s/def ::val integer?)

(s/def ::tag (s/and string?
                    #(re-matches #"^[a-zA-Z0-9\_][-a-zA-Z0-9\_\:]*" %)))

(s/def ::group (s/and string?
                      #(re-matches #"^[a-zA-Z0-9\_][-a-zA-Z0-9\_\:]*" %)))
