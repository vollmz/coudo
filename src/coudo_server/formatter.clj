(ns coudo-server.formatter
  (:require [clojure.string :as cstr]))

(defn- maplist-to-str [indexlst maplst result]
  (let [itm (first maplst)]
    (if (nil? itm)
      result
      (recur indexlst
             (rest maplst)
             (str result
                  (cstr/join ","
                             (map (fn [k] (get itm k "")) indexlst))
                  "\n")))))

(defn to-csv [data]
  (let [index (keys (first data))] ;; use first item of data to make csv index
    (maplist-to-str index
                    data
                    (str (cstr/join "," (map name index)) "\n"))))
