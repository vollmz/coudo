(ns coudo-server.app
  (:require [com.stuartsierra.component :as cpt]
            [reitit.ring :as reitit-ring]
            [ring.middleware.json :refer [wrap-json-body
                                          wrap-json-response]]
            [ring.middleware.params :refer [wrap-params]]
            [coudo-server.handler :as h])
  )

(defn- define-route [db user-config]
  [["/public/*" (reitit-ring/create-resource-handler)]
   ["/" {:get h/top}]
   ["/download/csv" {:get (partial h/download-csv db)}]
   ["/ping" {:get h/get-ping
             :post (partial h/post-ping user-config)}]
   ["/data/push" {:post (partial h/data-push db user-config)}]
   ["/data/update" {:post (partial h/data-update db user-config)}]
   ["/data/delete" {:post (partial h/data-delete db user-config)}]
   ["/data/list" {:get (partial h/data-list db)}]
   ["/data/totalize" {:get (partial h/data-totalize db)}]
   ["/group/make" {:post (partial h/group-make db user-config)}]
   ["/group/list" {:get (partial h/group-list db)}]
   ["/tag/getinfo" {:get (partial h/tag-getinfo db)}]
   ["/tag/count" {:get (partial h/tag-count db)}]
   ["/tag/gc" {:post (partial h/tag-gc db user-config)}]])

(defn- make-handler [db user-config]
  (reitit-ring/ring-handler
   (reitit-ring/router (define-route db user-config)
                       {:data {:middleware [wrap-json-response
                                            wrap-json-body
                                            wrap-params]}})
   (reitit-ring/routes (reitit-ring/redirect-trailing-slash-handler)
                       (reitit-ring/create-default-handler
                        {:not-found (constantly {:status 404 :body "Page not found"})
                         :method-not-allowed (constantly {:status 405 :body "Method not allowed"})
                         :not-acceptable (constantly {:status 406 :body "Not acceptable"})}))
   {}))

(defrecord App [handler db user-config]
  cpt/Lifecycle
  (start [this]
    (let [handler (make-handler (:datasource db)
                                user-config)]
      (assoc this :handler handler)))

  (stop [this]
    this))
