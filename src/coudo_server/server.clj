(ns coudo-server.server
  (:require [com.stuartsierra.component :as cpt]
            [ring.adapter.jetty :as jetty]))

(defrecord Server [instance server-config app]
  cpt/Lifecycle
  (start [this]
    (cond-> this
      (not instance) (assoc :instance (jetty/run-jetty (:handler app) server-config))))

  (stop [this]
    (when instance
      (.stop instance))
    (assoc this :instance nil)))
