(ns coudo-server.data
  (:require [clojure.spec.alpha :as s]
            [next.jdbc :as jdbc]
            [honey.sql.helpers :as h]
            [honey.sql :as sql]))

(defn unqualified [m]
  (into {} (map (fn [[k v]] [(keyword (name k)) v]) m)))

(defn insert-tag [db tagname]
  (with-open [con (jdbc/get-connection @db)]
    (let [result (jdbc/execute-one! con
                                    (-> (h/insert-into :tags)
                                        (h/columns :tagname)
                                        (h/values [[tagname]])
                                        (sql/format {:pretty true})))]
      (if (= (:next.jdbc/update-count result) 1) true false))))

(defn select-tid [db tagname]
  (with-open [con (jdbc/get-connection @db)]
    (let [result (jdbc/execute-one! con
                                    (-> (h/select :*)
                                        (h/from :tags)
                                        (h/where [:= :tagname tagname])
                                        (sql/format {:pretty true})))]
      (:tags/tid result))))

(defn get-tid [db tagname]
  (if-let [tid (select-tid db tagname)]
    tid
    (when (insert-tag db tagname)
      (select-tid db tagname))))

(defn count-tid [db tid]
  (with-open [con (jdbc/get-connection @db)]
    (let [result (jdbc/execute-one! con
                                    (-> (h/select :%count.*)
                                        (h/from :maindata)
                                        (h/where [:= :tid tid])
                                        (sql/format {:pretty true})))]
      (first (vals result)))))

(defn insert-maindata [db maindata]
  (with-open [con (jdbc/get-connection @db)]
    (let [result (jdbc/execute-one! con
                                    (-> (h/insert-into :maindata)
                                        (h/columns :author :tid :val)
                                        (h/values [[(:author maindata)
                                                    (:tid maindata)
                                                    (:val maindata)]])
                                        (sql/format {:pretty true})))]
      (if (= (:next.jdbc/update-count result) 1) true false))))

(defn update-maindata [db id updatedata]
  (with-open [con (jdbc/get-connection @db)]
    (let [result (jdbc/execute-one! con
                                    (-> (h/update :maindata)
                                        (h/set (into {} (map (fn [[k v]] [(keyword k) v])
                                                             updatedata)))
                                        (h/where [:= :id id])
                                        (sql/format {:pretty true})))]
      (if (= (:next.jdbc/update-count result) 1)
        true
        false))))

(defn delete-maindata [db id]
  (with-open [con (jdbc/get-connection @db)]
    (let [result (jdbc/execute-one! con
                                    (-> (h/delete-from :maindata) 
                                        (h/where [:= :id id])
                                        (sql/format {:pretty true})))]
      (if (= (:next.jdbc/update-count result) 1)
        true
        false))))

(defn delete-tag-and-group-association [db tids]
  (if (> (count tids) 0)
    (with-open [con (jdbc/get-connection @db)]
      (let [result1 (jdbc/execute-one! con
                                       (-> (h/delete-from :groups)
                                           (h/where [:in :tid tids])
                                           (sql/format {:pretty true})))
            result2 (jdbc/execute-one! con
                                       (-> (h/delete-from :tags)
                                           (h/where [:in :tid tids])
                                           (sql/format {:pretty true})))]
        {:update-groups (:next.jdbc/update-count result1)
         :update-tags (:next.jdbc/update-count result2)}))
    {:update-groups 0
     :update-tags 0}))

(defn insert-group [db groupdata]
  (with-open [con (jdbc/get-connection @db)]
    (let [result (jdbc/execute-one! con
                                    (-> (h/insert-into :groups)
                                        (h/columns :name :tid)
                                        (h/values [[(:group groupdata)
                                                    (:tid groupdata)]])
                                        (sql/format {:pretty true})))]
      (if (= (:next.jdbc/update-count result) 1) true false))))

(defn update-group [db gid groupdata]
  (with-open [con (jdbc/get-connection @db)]
    (let [result (jdbc/execute-one! con
                                    (-> (h/update :groups)
                                        (h/set {:name (:group groupdata)})
                                        (h/where [:= :gid gid])
                                        (sql/format {:pretty true})))]
      (if (= (:next.jdbc/update-count result) 1) true false))))

(defn get-tag-groupinfo [db tid]
  (with-open [con (jdbc/get-connection @db)]
    (jdbc/execute-one! con
                       (-> (h/select :*)
                           (h/from :groups)
                           (h/where [:= :tid tid])
                           (sql/format {:pretty true})))))

(defn select-grouplist [db]
  (with-open [con (jdbc/get-connection @db)]
    (jdbc/execute! con
                   (-> (h/select-distinct :name)
                       (h/from :groups)
                       (sql/format {:pretty true})))))

(defn select-taglist-of-groups [db grouplst]
  (with-open [con (jdbc/get-connection @db)]
    (jdbc/execute! con
                   (-> (h/select :tid)
                       (h/from :groups)
                       (h/where [:in :name grouplst])
                       (sql/format {:pretty true})))))

(defn select-taglist [db]
  (with-open [con (jdbc/get-connection @db)]
    (jdbc/execute! con
                   (-> (h/select :tid)
                       (h/from :tags)
                       (sql/format {:pretty true})))))

(defn select-maindata [db tids]
  (with-open [con (jdbc/get-connection @db)]
    (jdbc/execute! con
                   (-> (h/select :maindata.id
                                 :maindata.author
                                 :maindata.tid
                                 :tags.tagname
                                 [:groups.name :groupname]
                                 :maindata.val
                                 :maindata.recdate)
                       (h/from :maindata)
                       (h/join :tags [:= :maindata.tid :tags.tid])
                       (h/left-join :groups [:= :maindata.tid :groups.tid])
                       (h/where [:in :maindata.tid tids])
                       (sql/format {:pretty true})))))
