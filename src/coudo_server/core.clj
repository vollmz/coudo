(ns coudo-server.core
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.edn :as edn]
            [next.jdbc :as jdbc]
            [com.stuartsierra.component :as cpt]
            [coudo-server.db :as db]
            [coudo-server.app :as app]
            [coudo-server.server :as server])
  (:gen-class))

(defn init-system [config-data]
  (cpt/system-map
   :db (db/map->SqliteDb {:datasource nil :dbpath (-> (:datastorage config-data) :path)})
   :app (cpt/using (app/map->App {:user-config (:user config-data)}) [:db])
   :server (cpt/using (server/map->Server {:instance nil :server-config (:server config-data)})
                      [:app])))

(def cli-opts
  [["-c" "--config CONFIGFILE" "Specifiy config file" :default "coudo-config.edn"]
   ["-h" "--help" "Show help text"]])

(defn show-help [summary]
  (println "coudo-server")
  (println summary))

(defn load-config [pathstr]
  (edn/read-string (slurp pathstr)))

(def system (atom nil))

(defn start-program [config background-mode]
  (reset! system (cpt/start (init-system config))))

(defn stop-program []
  (reset! system (cpt/stop (deref system))))

(defn -main
  "Entry point of this program."
  [& args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-opts)]
    (cond
      (:help options) (show-help summary)
      :else (start-program (load-config (:config options))
                           (:background options)))))
