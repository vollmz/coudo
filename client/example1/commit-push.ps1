#!/bin/pwsh

Param(
  [switch]$ignoremerge
)

## Config

# accesskey
$accesskey = "testtest"

# URL
$server = "localhost:20080"
$gettaginfo = "http://${server}/tag/getinfo"
$grouping = "http://${server}/group/make"
$push = "http://${server}/data/push"

## Main

$ErrorActionPreference = "Stop"

# check the latest commit is merge commit.
$lastmergehash = git log -1 --merges --pretty=format:"%H"
$lastcommithash = git log -1 --pretty=format:"%H"
if ( ($lastmergehash -eq $lastcommithash) -and !$ignoremerge ) {
  Write-Output "Last commit is merge commit. Specify the -ignoremerge option to force it."
  exit
} elseif ( ($lastmergehash -eq $lastcommithash) -and $ignoremerge ){
  Write-Output "Last commit is not merge commit. Continue the process."
}

# Branch infos
$current = git rev-parse --abbrev-ref HEAD
$blst = git show-branch | Select-String "\*" | Select-String "${current}" -NotMatch | Select-Object -first 1 | %{ $_ -split("[]^~[]") }
$base = $blst[1]

$postdata = @{'tag' = $current}  | ConvertTo-Json -Compress
$postbody = [Text.Encoding]::UTF8.GetBytes($postdata)
try {
  $taggroupinfo = Invoke-RestMethod -Method GET -Uri "${gettaginfo}?tag=${current}"
} catch {
  Write-Output "Failed to get a taginfo"
  Write-Output "StatusCode:" $_.Exception.Response.StatusCode.value__
  Write-Error $_
}

if ( $taggroupinfo.group -ne $base ) {
  Write-Output "The group infomation is not match the current repository status."
  $postdata = @{'accesskey' = $accesskey; 'data' = @{'tag' = $current; 'group' = $base}} | ConvertTo-Json -Compress
  $postbody = [Text.Encoding]::UTF8.GetBytes($postdata)
  try {
    $groupedinfo = Invoke-RestMethod -Method POST -Uri $grouping -Body $postbody -ContentType application/json
  } catch {
    Write-Output "Failed to group the tag"
    Write-Output "StatusCode:" $_.Exception.Response.StatusCode.value__
    Write-Error $_
  }
  Write-Output "Changed => tag: ${current}, group: ${taggroupinfo.group} -> ${base}"
}

$rawdata = git diff HEAD~ HEAD --shortstat | %{ $_ -split "," }
$insert = 0
$delete = 0

foreach ($sep in $rawdata[1..$rawdata.length]) {
  $num = $sep.Trim().Split(" ")[0]
  if ( $num -match '^\d+$' ) {
    if ( $sep.Contains("insertions(+)") -or $sep.Contains("insertion(+)") ) {
      $insert = [int]$num
    }
    if ( $sep.Contains("deletions(-)") -or $sep.Contains("deletion(-)") ) {
      $delete = [int]$num
    }
  }
}

$modlines = $insert + $delete

if ( $insert -gt 0 ) {
  $author = git config --global user.name
  $postdata = @{'accesskey' = $accesskey; 'data' = @{'author' = $author; 'val' = $insert; 'tag' = $current}} | ConvertTo-Json -Compress
  $postbody = [Text.Encoding]::UTF8.GetBytes($postdata)
  try {
    $pushedinfo = Invoke-RestMethod -Method POST -Uri $push -Body $postbody -ContentType application/json
  } catch {
    Write-Output "Failed to push data"
    Write-Output "StatusCode:" $_.Exception.Response.StatusCode.value__
    Write-Error $_
  }

  Write-Output "Pushed => author: ${author}, tag:${current}, group: ${base}, Amount of +change: ${insert}"
} else {
  Write-Output "Push is canceled because the amount of +change is 0."
}

if ( $delete -gt 0 ) {
  $author = git config --global user.name
  $postdata = @{'accesskey' = $accesskey; 'data' = @{'author' = $author; 'val' = -$delete; 'tag' = $current}} | ConvertTo-Json -Compress
  $postbody = [Text.Encoding]::UTF8.GetBytes($postdata)
  try {
    $pushedinfo = Invoke-RestMethod -Method POST -Uri $push -Body $postbody -ContentType application/json
  } catch {
    Write-Output "Failed to push data"
    Write-Output "StatusCode:" $_.Exception.Response.StatusCode.value__
    Write-Error $_
  }

  Write-Output "Pushed => author: ${author}, tag:${current}, group: ${base}, Amount of -change: ${delete}"
} else {
  Write-Output "Push is canceled because the amount of -change is 0."
}
