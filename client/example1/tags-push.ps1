#!/bin/pwsh

Param (
  [String]$ftag, # Previous tag name
  [String]$ttag  # Latest tag name
)

## Config

# accesskey
$accesskey = "testtest"
$groupname = "release"
$author = "Admin"

# URL
$server = "localhost:20080"
$gettaginfo = "http://${server}/tag/getinfo"
$grouping = "http://${server}/group/make"
$push = "http://${server}/data/push"

## Main

$ErrorActionPreference = "Stop"

if ( ($ftag.Length -eq 0) -or ($ttag.Length -eq 0) ) {
  Write-Error "Specify 2 tag names."
  exit
}

# Get diff data
$rawdata = git diff $ftag $ttag --shortstat | %{ $_ -split "," }
$insert = 0
$delete = 0

foreach ($sep in $rawdata[1..$rawdata.length]) {
  $num = $sep.Trim().Split(" ")[0]
  if ( $num -match '^\d+$' ) {
    if ( $sep.Contains("insertions(+)") ) {
      $insert = [int]$num
    }
    if ( $sep.Contains("deletion(-)") ) {
      $delete = [int]$num
    }
  }
}

$modlines = $insert + $delete

# Prepare tag / Set group information
$tagname = $ttag
try {
  $taggroupinfo = Invoke-RestMethod -Method GET -Uri "${gettaginfo}?tag=${tagname}"
} catch {
  Write-Output "Failed to get a taginfo"
  Write-Output "StatusCode:" $_.Exception.Response.StatusCode.value__
  Write-Error $_
}

if ( $taggroupinfo.group -ne $groupname ) {
  $postdata = @{'accesskey' = $accesskey; 'data' = @{'tag' = $tagname; 'group' = $groupname}} | ConvertTo-Json -Compress
  $postbody = [Text.Encoding]::UTF8.GetBytes($postdata)
  try {
    $groupedinfo = Invoke-RestMethod -Method POST -Uri $grouping -Body $postbody -ContentType application/json
  } catch {
    Write-Output "Failed to group the tag"
    Write-Output "StatusCode:" $_.Exception.Response.StatusCode.value__
    Write-Error $_
  }
  Write-Output "=> tag: ${tagname}, group: ${taggroupinfo.group} -> ${groupname}"
}

# Push
$postdata = @{'accesskey' = ${accesskey}; 'data' = @{'author' = ${author}; 'val' = ${modlines}; 'tag' = ${tagname}}} | ConvertTo-Json -Compress
$postbody = [Text.Encoding]::UTF8.GetBytes($postdata)
try {
  $pushedinfo = Invoke-RestMethod -Method POST -Uri $push -Body $postbody -ContentType application/json
} catch {
  Write-Output "Failed to push data"
  Write-Output "StatusCode:" $_.Exception.Response.StatusCode.value__
  Write-Error $_
}

Write-Output "Pushed => author: ${author}, tag:${tagname}, group: ${base}, Amount of change: ${modlines}"





