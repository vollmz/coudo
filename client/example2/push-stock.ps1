#!/bin/pwsh

## Config
$store = "/tmp/store.csv"
$stored_ext = ".pushed"

$accesskey = "testtest"

$server = "localhost:20080"
$gettaginfo = "http://${server}/tag/getinfo"
$grouping = "http://${server}/group/make"
$push = "http://${server}/data/push"

$username = git config --global user.name

## Main

$ErrorActionPreference = "Stop"

function push_data($author, $group, $tag, $val) {
    $postdata = @{'tag' = $tag}  | ConvertTo-Json -Compress
    $postbody = [Text.Encoding]::UTF8.GetBytes($postdata)
    try {
        $taggroupinfo = Invoke-RestMethod -Method GET -Uri "${gettaginfo}?tag=${tag}"
    } catch {
        Write-Output "Failed to get a taginfo"
        Write-Output "StatusCode:" $_.Exception.Response.StatusCode.value__
        Write-Error $_
    }
    $server_group = $taggroupinfo.group

    if ( $server_group -ne $group ) {
        Write-Output "The group infomation is not match the current repository status."
        $postdata = @{'accesskey' = $accesskey; 'data' = @{'tag' = $tag; 'group' = $group}} | ConvertTo-Json -Compress
        $postbody = [Text.Encoding]::UTF8.GetBytes($postdata)
        try {
            $groupedinfo = Invoke-RestMethod -Method POST -Uri $grouping -Body $postbody -ContentType application/json
        } catch {
            Write-Output "Failed to group the tag"
            Write-Output "StatusCode:" $_.Exception.Response.StatusCode.value__
            Write-Error $_
        }
        Write-Output "Changed => tag: ${tag}, group: ${server_group} -> ${group}"
    }

    $postdata = @{'accesskey' = $accesskey; 'data' = @{'author' = $author; 'val' = $val; 'tag' = $tag}} | ConvertTo-Json -Compress
    $postbody = [Text.Encoding]::UTF8.GetBytes($postdata)
    try {
        echo $postdata
        $pushedinfo = Invoke-RestMethod -Method POST -Uri $push -Body $postbody -ContentType application/json
    } catch {
        Write-Output "Failed to push data"
        Write-Output "StatusCode:" $_.Exception.Response.StatusCode.value__
        Write-Error $_
    }

    Write-Output "Pushed => author: ${author}, tag: ${tag}, group: ${group}, val: ${val}"
}

$data = Import-Csv $store -Encoding UTF8

foreach ($col in $data) {
    $group = $col.group
    $tag = $col.tag
    $insert = [int]$col.inserts
    $delete = [int]$col.deletes

    if ($insert -gt 0) {
        push_data $username $group $tag $insert
    }

    if ($delete -lt 0) {
        push_data $username $group $tag $delete
    }
}


$nowtime = Get-Date -UFormat '%Y%m%d%H%M%S'
Move-Item $store ($store + "." + $nowtime + $stored_ext)
