#!/bin/pwsh

Param(
  [switch]$ignoremerge
)

## Config
$store = "/tmp/store.csv"

## Main

$ErrorActionPreference = "Stop"

# check the latest commit is merge commit.
$lastmergehash = git log -1 --merges --pretty=format:"%H"
$lastcommithash = git log -1 --pretty=format:"%H"
if ( ($lastmergehash -eq $lastcommithash) -and !$ignoremerge ) {
  Write-Output "Last commit is merge commit. Specify the -ignoremerge option to force it."
  exit
} elseif ( ($lastmergehash -eq $lastcommithash) -and $ignoremerge ){
  Write-Output "Last commit is not merge commit. Continue the process."
}

# Branch infos
$current = git rev-parse --abbrev-ref HEAD
$blst = git show-branch | Select-String "\*" | Select-String "${current}" -NotMatch | Select-Object -first 1 | %{ $_ -split("[]^~[]") }

if ( -not $blst ) {
    Write-Output "A parent branch does not found."
    exit
}

$base = $blst[1]

$rawdata = git diff HEAD~ HEAD --shortstat | %{ $_ -split "," }
$insert = 0
$delete = 0

foreach ($sep in $rawdata[1..$rawdata.length]) {
  $num = $sep.Trim().Split(" ")[0]
  if ( $num -match '^\d+$' ) {
    if ( $sep.Contains("insertions(+)") -or $sep.Contains("insertion(+)") ) {
      $insert = [int]$num
    }
    if ( $sep.Contains("deletions(-)") -or $sep.Contains("deletion(-)") ) {
      $delete = [int]$num
    }
  }
}

$outputdata = @(
    [pscustomobject]@{
        group = $base
        tag = $current
        inserts = $insert
        deletes = -$delete
    }
)

if ($insert -gt 0 -or $delete -gt 0) {
    $outputdata | Export-Csv -NoTypeInformation $store -Encoding UTF8 -Append
    Write-Output "Group ${base}, Tag ${current}, Inserts ${insert}, Deletes ${delete}"
}

